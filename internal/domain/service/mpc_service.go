package service

import (
	"github.com/arpachain/coordinator/internal/domain/entity"
	"github.com/arpachain/coordinator/internal/domain/repository"
)

type MPCService struct {
	repository repository.MPCRepository
}

func NewMPCService(repository repository.MPCRepository) *MPCService {
	return &MPCService{
		repository: repository,
	}
}

func (s *MPCService) Validate(MPC entity.MPC) error {
	return nil
}
