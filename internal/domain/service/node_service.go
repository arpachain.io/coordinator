package service

import (
	"github.com/arpachain/coordinator/internal/domain/entity"
	"github.com/arpachain/coordinator/internal/domain/repository"
)

type NodeService struct {
	repository repository.NodeRepository
}

func NewNodeService(repository repository.NodeRepository) *NodeService {
	return &NodeService{
		repository: repository,
	}
}

func (s *NodeService) Validate(node entity.Node) error {
	return nil
}
