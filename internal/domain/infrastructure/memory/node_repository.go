package memory

import (
	"sync"

	"github.com/arpachain/coordinator/internal/domain/entity"
)

type nodeRepository struct {
	mu    *sync.Mutex
	nodes map[string]*entity.Node
}

func NewNodeRepository() *nodeRepository {
	return &nodeRepository{
		mu:    &sync.Mutex{},
		nodes: map[string]*entity.Node{},
	}
}

func (r *nodeRepository) FindAll() ([]*entity.Node, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	nodes := make([]*entity.Node, len(r.nodes))
	i := 0
	for _, node := range r.nodes {
		nodes[i] = entity.NewNode(node.ID, node.IP, node.Port, node.Available)
		i++
	}
	return nodes, nil
}

func (r *nodeRepository) FindByID(id string) (*entity.Node, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	for _, node := range r.nodes {
		if node.ID == id {
			return entity.NewNode(node.ID, node.IP, node.Port, node.Available), nil
		}
	}
	return nil, nil
}

func (r *nodeRepository) Save(node *entity.Node) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.nodes[node.GetID()] = &entity.Node{
		ID:        node.GetID(),
		IP:        node.GetIP(),
		Port:      node.GetPort(),
		Available: node.GetAvailability(),
	}
	return nil
}

func (r *nodeRepository) Update(id string, ip, port uint32, available bool) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.nodes[id] = &entity.Node{
		ID:        id,
		IP:        ip,
		Port:      port,
		Available: available,
	}
	return nil
}
