package memory

import (
	"sync"

	"github.com/arpachain/coordinator/internal/domain/entity"
)

type mpcRepository struct {
	mu   *sync.Mutex
	mpcs map[string]*entity.MPC
}

func NewMPCRepository() *mpcRepository {
	return &mpcRepository{
		mu:   &sync.Mutex{},
		mpcs: map[string]*entity.MPC{},
	}
}

func (r *mpcRepository) FindAll() ([]*entity.MPC, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	mpcs := make([]*entity.MPC, len(r.mpcs))
	i := 0
	for _, mpc := range r.mpcs {
		mpcs[i] = entity.NewMPC(mpc.ID, mpc.NodeID)
		i++
	}
	return mpcs, nil
}

func (r *mpcRepository) FindByID(id string) (*entity.MPC, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	for _, mpc := range r.mpcs {
		if mpc.ID == id {
			return entity.NewMPC(mpc.ID, mpc.NodeID), nil
		}
	}
	return nil, nil
}

func (r *mpcRepository) Save(mpc *entity.MPC) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.mpcs[mpc.GetID()] = &entity.MPC{
		ID: mpc.GetID(),
	}
	return nil
}

func (r *mpcRepository) Update(mpcID, nodeID string, finished bool, result *entity.MPCResult) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.mpcs[mpcID] = &entity.MPC{
		ID:       mpcID,
		NodeID:   nodeID,
		Finished: finished,
		Result:   result,
	}
	return nil
}
