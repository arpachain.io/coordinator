package repository

import "github.com/arpachain/coordinator/internal/domain/entity"

type NodeRepository interface {
	FindAll() ([]*entity.Node, error)
	FindByID(id string) (*entity.Node, error)
	Save(*entity.Node) error
	Update(id string, ip, port uint32, available bool) error
}
