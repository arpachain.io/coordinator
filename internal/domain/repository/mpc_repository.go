package repository

import "github.com/arpachain/coordinator/internal/domain/entity"

type MPCRepository interface {
	FindAll() ([]*entity.MPC, error)
	FindByID(id string) (*entity.MPC, error)
	Save(*entity.MPC) error
	Update(mpcID, nodeID string, finished bool, result *entity.MPCResult) error
}
