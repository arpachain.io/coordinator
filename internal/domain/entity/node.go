package entity

// Node as a single MPC node
type Node struct {
	ID        string
	IP        uint32
	Port      uint32
	Available bool
}

func NewNode(id string, ip, port uint32, available bool) *Node {
	return &Node{
		ID:        id,
		IP:        ip,
		Port:      port,
		Available: available,
	}
}

func (n *Node) GetID() string {
	return n.ID
}

func (n *Node) GetIP() uint32 {
	return n.IP
}

func (n *Node) GetPort() uint32 {
	return n.Port
}

func (n *Node) GetAvailability() bool {
	return n.Available
}
