package entity

type MPCResult struct {
	Data string
}

func NewMPCResult(data string) *MPCResult {
	return &MPCResult{
		Data: data,
	}
}

func (m *MPCResult) GetData() string {
	return m.Data
}
