package entity

// MPC as a single MPC execution
type MPC struct {
	ID       string
	NodeID   string
	Finished bool
	Result   *MPCResult
}

func NewMPC(mpcID, nodeID string) *MPC {
	return &MPC{
		ID:       mpcID,
		NodeID:   nodeID,
		Finished: false,
	}
}

func (m *MPC) GetID() string {
	return m.ID
}

func (m *MPC) GetStatus() bool {
	return m.Finished
}

func (m *MPC) GetResult() *MPCResult {
	return m.Result
}
