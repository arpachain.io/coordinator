package usecase

import (
	"github.com/arpachain/coordinator/internal/domain/entity"
	"github.com/arpachain/coordinator/internal/domain/repository"
	"github.com/arpachain/coordinator/internal/domain/service"
)

type NodeUsecase interface {
	List() ([]*entity.Node, error)
	Register(ip, mac string) error
}

type nodeUsecase struct {
	repository repository.NodeRepository
	service    *service.NodeService
}

func NewNodeUsecase(repository repository.NodeRepository, service *service.NodeService) *nodeUsecase {
	return &nodeUsecase{
		repository: repository,
		service:    service,
	}
}

func (u *nodeUsecase) List() ([]*entity.Node, error) {
	nodes, error := u.repository.FindAll()
	if error != nil {
		return nil, error
	}
	return nodes, nil
}

func (u *nodeUsecase) Register(ip, port uint32) (string, error) {
	id, err := generateId()
	if err != nil {
		return "", err
	}
	node := entity.NewNode(id, ip, port, true)
	if err := u.repository.Save(node); err != nil {
		return "", err
	}
	return id, nil
}
