package usecase

import (
	"github.com/arpachain/coordinator/internal/domain/entity"
	"github.com/arpachain/coordinator/internal/domain/repository"
	"github.com/arpachain/coordinator/internal/domain/service"
)

type MPCUsecase interface {
	List() ([]*entity.MPC, error)
	Create(nodeID string) error
	Finish(mpcID, nodeID string, result entity.MPCResult) error
}

type mpcUsecase struct {
	repository repository.MPCRepository
	service    *service.MPCService
}

func NewMPCUsecase(repository repository.MPCRepository, service *service.MPCService) *mpcUsecase {
	return &mpcUsecase{
		repository: repository,
		service:    service,
	}
}

func (u *mpcUsecase) List() ([]*entity.MPC, error) {
	MPCs, error := u.repository.FindAll()
	if error != nil {
		return nil, error
	}
	return MPCs, nil
}

func (u *mpcUsecase) Create(nodeID string) error {
	id, err := generateId()
	if err != nil {
		return err
	}
	MPC := entity.NewMPC(id, nodeID)
	if err := u.repository.Save(MPC); err != nil {
		return err
	}
	return nil
}

func (u *mpcUsecase) Finish(nodeID, mpcID string, result *entity.MPCResult) error {
	return u.repository.Update(nodeID, mpcID, true, result)
}
