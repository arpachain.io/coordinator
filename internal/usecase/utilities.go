package usecase

import "github.com/rs/xid"

func generateId() (string, error) {
	id := xid.New()
	return id.String(), nil
}
