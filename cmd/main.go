package main

import (
	"log"
	"net"

	"github.com/arpachain/coordinator/internal/domain/entity"

	pb "github.com/arpachain/coordinator/api/mpc-grpc"
	"github.com/arpachain/coordinator/internal/domain/infrastructure/memory"
	"github.com/arpachain/coordinator/internal/domain/service"
	"github.com/arpachain/coordinator/internal/usecase"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50051"
)

// server is used to implement arpa.Coordinator.
type server struct{}

// Need to refactor the code to remove unecessary abstraction layers
// RegisterMPCNode implements arpa.Coordinator.RegisterMPCNode
func (s *server) RegisterMPCNode(ctx context.Context, in *pb.RegisterMPCNodeRequest) (*pb.RegisterMPCNodeResponse, error) {
	nodeRepository := memory.NewNodeRepository()
	nodeService := service.NewNodeService(nodeRepository)
	nodeUsecase := usecase.NewNodeUsecase(nodeRepository, nodeService)
	nodeID, err := nodeUsecase.Register(in.GetIpAddr(), in.GetPort())
	if err != nil {
		return &pb.RegisterMPCNodeResponse{
			NodeId:  nodeID,
			Success: false,
		}, err
	}
	return &pb.RegisterMPCNodeResponse{
		NodeId:  nodeID,
		Success: true,
	}, nil
}

// Need to finish the implementation later
// FinishMPC implements arpa.Coordinator.FinishMPC
func (s *server) FinishMPC(ctx context.Context, in *pb.FinishMPCRequest) (*pb.FinishMPCResponse, error) {
	mpcRepository := memory.NewMPCRepository()
	mpcService := service.NewMPCService(mpcRepository)
	mpcUsecase := usecase.NewMPCUsecase(mpcRepository, mpcService)
	mpcResult := entity.NewMPCResult(in.GetResult().GetData())
	err := mpcUsecase.Finish(in.GetNodeId(), in.GetMpcId(), mpcResult)
	if err != nil {
		return &pb.FinishMPCResponse{
			Success: false,
		}, err
	}
	return &pb.FinishMPCResponse{
		Success: true,
	}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterCoordinatorServer(s, &server{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
