protoc -I ~/go/src/github.com/arpachain/coordinator/api ~/go/src/github.com/arpachain/coordinator/api/mpc-grpc/coordinator.proto --go_out=plugins=grpc:api
protoc -I ~/go/src/github.com/arpachain/coordinator/api ~/go/src/github.com/arpachain/coordinator/api/mpc-grpc/node.proto --go_out=plugins=grpc:api
